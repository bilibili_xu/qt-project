#ifndef MASKLABEL_H
#define MASKLABEL_H

#include <QWidget>
#include <QLabel>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QTimer>
#include <QTime>

class MaskLabel : public QLabel
{
    Q_OBJECT
public:
    MaskLabel(int pos_x,QString fans_name,QWidget*parent = Q_NULLPTR);
    QPropertyAnimation * animation_fans;
    ~MaskLabel();


};

#endif // MASKLABEL_H
